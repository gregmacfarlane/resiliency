Properties
==============================
This folder contains data on the home values of Atlanta parcels from 2002
through 2012. There are three primary folders:

  - `appraised_values/` The appraised values of all homes in Fulton County, broken into several data files as they came from the county tax assessor.
  - `parcels/` A shapefile with the spatial geometry of all parcels in Fulton County.
  - `shp/` various other Atlanta shapefiles with application to the study.
