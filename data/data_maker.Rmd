---
title: "Resiliency Data Maker" 
author: "Gregory Macfarlane | Georgia Tech"
output:  html_document
---

This document creates an analysis `SpatialPointsDataFrame` from source data
obtained from Fulton County through an open records request.

```{r libraries, message=FALSE, warning=FALSE}
library(knitr)
opts_chunk$set(echo=TRUE, cache = TRUE, cache.path = ".data_cache/", 
               tidy=FALSE)
dep_auto()

# Data Manipulation libraries
library(dplyr)
library(readr)
library(stringr)

# Spatial libraries
library(spdep)
library(maptools)
library(rgdal)

# census API function
source("../R/call_census_api.R")
```


# Assessment Data

The Fulton county tax assessor's office provided assessed values for 2002-2012 
as a set of `.txt` files. We want to load all of these and join the relevant
variables together into a single data frame.

```{r readtextfiles, warning=FALSE}
# get field names and types
source("properties/field_names.R")

# get list of csv files
textfiles <- list.files(path="./properties/appraised_values", 
                        full.names = TRUE, pattern = '*.csv')

df <- lapply(
    textfiles, function(x)  
    read_csv(x, skip = 1,  col_names = field_names, col_types = field_types)
  ) %>%
  rbind_all() %>%
  group_by(parid) %>% 
  arrange(taxyr)
```

There is some filtering we want to do; we need to restrain the analysis to
single-unit buildings that we have a value for in every year.

```{r filter_data}
df <- df %>%
  # single units
  filter(livunit == 1) %>%
  
  # reasonable build dates
  filter(d_yrblt > 1800 & d_yrblt < 2014) %>%
  # some properties coded incorrectly as 2013
  mutate(taxyr = ifelse(taxyr == 2013L, 2012L, taxyr)) %>%
  filter(taxyr > 2002) %>%
  
  # no repeat records
  group_by(parid, taxyr) %>%
  slice(1) %>%
  
  # complete records
  group_by(parid) %>%
  arrange(taxyr) %>%
  filter(n() == 10)
```


## Volatility

`dplyr` has very convenient window function that make calculating the 
volatility values trivial.

Because it is possible that some years have some variables coded incorrectly
(this is very rare by visual inspection), we will take the median of each
variable by `parid`. In the event that one or two years are miscoded, the
majority of the years will be correct and the median will be accurate. This is
reasonable because the following items are assumed to not change:

  1. Property acres
  2. Construction year
  3. Effective home year (based on air conditioning, plumbing, etc.)
  3. Total rooms, bedrooms, and bathrooms

```{r volatility}
df <- df %>%
  mutate( 
    indexed_value = aprtot / aprtot[1],
    growth_rate = (lead(indexed_value) - indexed_value) / indexed_value,
    age = ifelse(is.na(d_effyr), 2014 - d_yrblt, 2014 - d_effyr)
  ) 

# save raw numbers for plotting
saveRDS(df %>% select(parid, indexed_value), "./timeseries_values.rds")

# roll up to the household
df <- df %>%
  summarise(
    # y variables
    mean_val = mean(indexed_value),
    mean_grt = mean(growth_rate, na.rm = TRUE),
    sdev_grt = sd(growth_rate, na.rm = TRUE),
    # x variables
    acres = median(calcacres, na.rm = TRUE),
    age   = median(age, na.rm = TRUE),
    rooms = median(rmtot, na.rm = TRUE),
    beds  = median(rmbed, na.rm = TRUE), 
    baths = median(fixbath + fixhalf * 0.5, na.rm = TRUE)
  ) %>%
  filter(complete.cases(.))
```


## Sampling
Right now the `spdep` doesn't work so great on more than 5000 observations. So
we sample 1000 randomly to test the code.

```{r sampling}
df <- df %>% sample_n(5000)
```


# Geographic Calculations

Some of the variables we want to use are based on the home's location, and are
not stored in the assessment data.

To locate the home, we use a polygon file for all parcels in Fulton county, each
parcel, to which we join the price volatility metrics and home attributes we
calculated above. The location of the parcel for our purposes is the polygon
centroid.

  
```{r shapefiles}
# West Georgia state plane (USFT)
proj <- CRS("+proj=tmerc +lat_0=30 +lon_0=-84.16666666666667 +k=0.9999
            +x_0=699999.9998983998 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0
            +units=us-ft +no_defs")

# Load parcels file, extract centroid coordinates, and append to points as a 
# SpatialPointsDataFrame
parcels_shp <- readShapeSpatial("./properties/parcels/Tax_Parcels.shp",
                                proj4string = proj, delete_null_obj = TRUE)
parcels_xy  <- as.data.frame(coordinates(parcels_shp))
names(parcels_xy) <- c("x", "y")
parcels_xy$parid <- parcels_shp@data$ParcelID
rm(parcels_shp)

df <- df %>%
  inner_join(parcels_xy)

df_pts <- SpatialPointsDataFrame(
  coords = cbind(df$x, df$y),  data = as.data.frame(df), 
  proj4string = proj, match.ID = FALSE)
```

## Census variables
Some information that we want to model is based on the neighborhood, or Census
tract.  We identify which tract each house was in for the 2000 and 2010 censuses
using spatial overlay functions.^[Because the tract definitions changed
slightly between censuses, and the names changed completely.]

```{r appendtract}
WGS84 <- CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs +towgs84=0,0,0")

# 2010 Tracts
Tract2010 <- readShapePoly(
  "./census/gz_2010_13_140_00_500k/gz_2010_13_140_00_500k.shp",
  proj4string = WGS84) %>%
  spTransform(., proj)
Tract2010 <- Tract2010[which(Tract2010@data$COUNTY == "121"), ]

Tract2010@data <- Tract2010@data %>%
  transmute(geoid = paste(STATE, COUNTY, TRACT, sep = ""))

# 2000 Tracts
Tract2000 <- readShapePoly(
  "./census/tr13_d00_shp/tr13_d00.shp",  
  proj4string = WGS84) %>%
  spTransform(., proj) 
  
Tract2000 <- Tract2000[which(Tract2000@data$COUNTY == "121"), ]

Tract2000@data <- Tract2000@data %>%
  transmute(
    geoid = paste(STATE, COUNTY, TRACT, sep = ""),
    geoid = ifelse(nchar(geoid) != 11, paste(geoid, "00", sep = ""), geoid)
  )

# spatial join
df_pts@data$tract_00 <- over(df_pts, Tract2000)$geoid
df_pts@data$tract_10 <- over(df_pts, Tract2010)$geoid
```


### Neighborhood Income Growth

A competing explanation for changes in property values is gentrification; we
therefore would like a measurement of this. The simplest method would be to
measure change in resident income over time. We use median income collected for
the 2000 Census and the 2008-2013 ACS, gathered directly from the census data
API using a function stored in `R/call_census_api`^[This is a modified version 
of a function written by Josephine Kressner for 
[Transport Foundry](http://transportfoundry.com)]


```{r get_census_vars}
# load income tables
income_10 <- call_census_api("B19013_001E", Tract2010$geoid, 2010)
income_00 <- call_census_api("HCT012001", Tract2000$geoid, 2000)

names(income_10) <- c("tract_10", "income_10")
names(income_00) <- c("tract_00", "income_00")

df_pts@data <- df_pts@data %>%
  left_join(income_00) %>%
  left_join(income_10) %>%
  
  # calculate income growth
  mutate(
    income_00 = as.numeric(income_00) * 1.35, # adj for inflation, 2000 to 2013
    income_10 = as.numeric(income_10),
    income_grt = (as.numeric(income_10) - income_00) / income_00
  )
```

### Neighborhood Diversity

Another variable we want is the percent of white households as a measure of
neighborhood diversity.

```{r nonwhite}
# get total estimate and estimate of white
hh <- call_census_api(c("B02001_001E", "B02001_002E"), 
                      Tract2010$geoid, 2010) %>%
  transmute(
    tract_10 = geoid,
    pct_white = as.numeric(B02001_002E) / as.numeric(B02001_001E)
    )

df_pts@data <- df_pts@data %>%
  left_join(hh)
```



## Distance Calculations

We need to calculate the distance between each house and its nearest rail
station. This code chunk does this by selecting the minimum station measured
with the `spDistsN1` function. We also append the name of the station, and flag
if that station has a park-and-ride, which we take as a measure of urbanness.

```{r marta_dist}
marta_pts <- readShapeSpatial("properties/shp/MARTA_Stations.shp",
                              proj4string = proj)

marta_dist <- lapply(1:length(df_pts), function(x) 
  spDistsN1(marta_pts, df_pts[x,]))

df_pts$marta_dist <- sapply(marta_dist, min)

df_pts$marta_nearest <- sapply(marta_dist, function(x)
  marta_pts$STATION[which.min(x)])  

# Flag if a park and ride exists
noparkandrides <- c(
  "Five Points", "Peachtree Center", "Civic Center",  "North Avenue", 
  "Midtown", "Arts Center", "Lindbergh Center", #technically yes, but very urban 
  "Buckhead", "Dome/GWCC/Philips/CNN", "Bankhead", "Georgia State", 
  "King Memorial", "Decatur", "Garnett"
)

df_pts$pnr <- ifelse(df_pts$marta_nearest %in% noparkandrides, 0, 1)
```

This code chunk counts measures the distance between the parcels and the freeway
interchanges, and then counts the number of interchanges that are nearer than
the `DIST_RAILSTOP` variable calculated in the previous chunk. This is a way to
account for transportation accessibility in a way that is not entirely collinear
with the simple distance to the interchange.

```{r intchg_dist}
intchg_pts <- readShapeSpatial("properties/shp/Interchanges_Proj.shp",
                               proj4string = proj)

intchg_dist <- lapply(1:length(df_pts), function(x) 
  spDistsN1(intchg_pts, df_pts[x, ])) 

df_pts$intchg_dist <- sapply(intchg_dist, min)
df_pts$intchg_num <- unlist(lapply(1:length(df_pts), function(x)
  sum(intchg_dist[[x]] < df_pts$marta_dist[x])))
```

# Save
The saved data file is `SpatialPointsDataFrame` suitable for modeling with 
`spdep`.
```{r publicexport}
saveRDS(df_pts, file = "resiliency_data.rds")
```

```{r session_info}
sessionInfo()
```

