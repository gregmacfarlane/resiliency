Resiliency
====================

This is the repository for *The association between public transportation
infrastructure and home price growth and stability*, a study and paper by 
Gregory Macfarlane and Juan Moreno-Cruz, of Parsons Brinckerhoff and Georgia Tech
respectively. 

There are two primary document:
  - `data/data_maker.Rmd`: This R markdown document builds an analysis dataset
  from source data that is included in the repository or downloaded in-stream
  from the Census API.
  
  - `doc/resiliency.rnw`: This is a `knitr` document containing the analysis
  code and the text of the paper.
  
There is also an `R` folder containing various functions and helper scripts that
both documents call as needed.

The easiest way to build either document is to use RStudio's built-in markdown/TeX 
compilers.
  
Data Maker
-----------------
Before building this document, the user should expand the ZIP files located in
`data/census/` and `data/properties`.

This document takes about 40 minutes to build, largely spent reading in the 
polygon shapefile of all parcels in Fulton County. When finished, there
should be two new files in `data/`

  - `data/resiliency_data.rds` The primary analysis dataset.
  - `timeseries_values.rds` A dataset preserving the asssesed value by time 
  for Figure 3.
  

Document
-----------------
Before building this document, build `data/data_maker.Rmd`.

This document takes more than an hour to build, almost all of which is estimating
the spatial dependence models. If you are testing something in the document,
you might sample down the `df_pts` spatial points object. In the `loaddata`
chunk, simply include

    df_pts <- readRDS("../data/resiliency_data.rds") 
    df_pts <- df_pts[complete.cases(df_pts@data), ]
    df_pts <- df_pts[sample(1:nrow(df_pts), 1500), ]


When the dataset contains around 1500 observation, it builds in only a few minutes.